package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyBagPage extends BasePage {
    @FindBy(xpath = "//span[@aria-label='Quantity']")
    private WebElement quantityDropdown;

    @FindBy(xpath = "//li[contains(text(), '6')]")
    private WebElement quantityIsSix;

    @FindBy(xpath = "//span[@title='6']")
    private WebElement quantitySixInMyBag;

    @FindBy(xpath = "//h2[@class='empty-bag-title']")
    private WebElement bagIsEmpty;

    @FindBy(xpath = "//button[@class='bag-item-remove']")
    private WebElement deleteButton;

    public MyBagPage(WebDriver driver){
        super(driver);
    }

    public void selectQuantityOfShirtsIsSix() {
        waitToBeClickableOfElement(quantityDropdown);
        clickElement(quantityDropdown);
        clickElement(quantityIsSix);
    }

    public boolean checkQuantityIsSixIsDisplayed() {
        return quantitySixInMyBag.isDisplayed();
    }

    public void clickDeleteButton() {
        waitToBeClickableOfElement(deleteButton);
        clickElement(deleteButton);
    }

    public boolean checkBagIsEmptyMessageIsDisplayed() {
        waitVisibilityOfElement(bagIsEmpty);
        return bagIsEmpty.isDisplayed();
    }
}
