package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MenPage extends BasePage {
    @FindBy(xpath = "//a[contains(@href,'men/shirts') and text()='Shirts']")
    private WebElement shirtsCategory;

    @FindBy(xpath = "//nav[not(@aria-hidden='true')]//button[@data-index='3']")
    private WebElement clothingMenCategory;

    public MenPage(WebDriver driver){
        super(driver);
    }

    public void clickClothingMenCategory(){
        clickElement(clothingMenCategory);
    }

    public void clickShirtsCategory(){
        clickElement(shirtsCategory);
    }

}
