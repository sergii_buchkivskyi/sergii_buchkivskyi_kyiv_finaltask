import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {
    private static final long DEFAULT_TIMEOUT = 30;
    WebDriver driver;
    HomePage homePage;
    MenPage menPage;
    ShirtsPage shirtsPage;
    SavedItemsPage savedItemsPage;
    SignUpPage signUpPage;
    SignInPage signInPage;
    MyBagPage myBagPage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string} page")
    public void  openHomePage(String url){
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks search field visibility")
    public void userChecksSearchFieldVisibility() {
        homePage.searchFieldIsVisibility();
    }

    @When("User makes search by keyword {string}")
    public void userMakesSearchByKeyword (String keyword) {
        homePage.inputWordToSearchField(keyword);
    }

    @Then("User checks that url contains {string}")
    public void userChecksThatResultsContainsKeyword(String searchQuery) {
        assertTrue(homePage.checkThatUrlContainsSearchWord(searchQuery));
    }

    @Then("User checks that nothing matches your search message is displayed")
    public void userChecksThatNothingMatchesYourSearchMessageIsDisplayed() {
        assertTrue(homePage.checkNothingMatchesYourSearchMessageIsDisplay());
    }

    @Then("User checks that amount of products on search result page are {int}")
    public void userChecksThatAmountOfProductsEqualsAmountFromSearchResultPage(int amount) {
        assertTrue(homePage.checkThatAmountOfProductsEqualsAmountFromSearchResultPage(amount));
    }

    @And("User clicks men category")
    public void userClicksMenCategory() {
        homePage.clickMenCategory();
    }

    @And("User clicks clothing category")
    public void userClicksClothingCategory() {
        menPage = pageFactoryManager.getMenPage();
        menPage.clickClothingMenCategory();
    }

    @When("User clicks add to saved items button")
    public void userClickAddToSavedItemsButton() {
        homePage.clickAddToSavedItemsButton();
    }

    @And("User clicks saved items button")
    public void userClickSavedItemsButton() {
        homePage.clickSavedItemsButton();
    }

    @Then("User checks that item is added to saved items")
    public void userChecksThatItemIsAddedToSavedItems() {
        savedItemsPage = pageFactoryManager.getSavedItemsPage();
        assertTrue(savedItemsPage.checkAddingToSavedItems());
    }

    @When("User clicks add to bag button")
    public void userClickAddToBagButton() {
        shirtsPage.clickAddToBagButton();
    }

    @And("User selects {string} of shirt")
    public void userSelectsSizeOfShirt(String size) {
        shirtsPage.chooseSizeOfShirt(size);
    }

    @Then("User checks that number 1 is appeared on the bag icon")
    public void userCheckThatOneIsAppearedOnBagIcon() {
        homePage.implicitWait(DEFAULT_TIMEOUT);
        assertTrue(homePage.itemIsAddedToBag());
    }

    @And("User clicks myAccount button")
    public void userClicksMyAccountButton() {
        homePage.clickMyAccountButton();
    }

    @And("User clicks join link")
    public void userClicksJoinLink() {
        homePage.clickJoinLink();
    }

    @And("User inputs email {string}")
    public void userInputsEmailAddress(String email) {
        signUpPage = pageFactoryManager.getSignUpPage();
        signUpPage.inputEmailAddress(email);
    }

    @And("User inputs first {string}")
    public void userInputsFirstName(String firstName) {
        signUpPage.inputFirstName(firstName);
    }

    @And("User inputs last {string}")
    public void userInputsLastLName(String lastName) {
        signUpPage.inputLastName(lastName);
    }

    @And("User inputs {string}")
    public void userInputsPassword(String password) {
        signUpPage.inputPassword(password);
    }

    @And("User selects data of birth")
    public void userSelectDataOfBirth() {
        signUpPage.selectDataOfBirth();
    }

    @When("User clicks join asos button")
    public void userClicksJoinAsosButton() {
        signUpPage.clickJoinAsosButton();
    }


    @And("User clicks sign in link")
    public void userClicksSignInLink() {
        homePage.clickSignInLink();
    }

    @And("User inputs signInEmail {string}")
    public void userInputsSignInEmailAddress(String email) {
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.inputEmailAddress(email);
    }

    @And("User inputs signInPassword {string}")
    public void userInputsSignInPasswordPassword(String password) {
        signInPage.inputPassword(password);
    }

    @When("User clicks sign in button")
    public void userClicksSignInButton() {
        signInPage.clickSignInButton();
    }

    @When("User checks that passwordSignInErrorMessage is displayed")
    public void userChecksThatLoginErrorMessageIsDisplayed() {
        assertTrue(signInPage.loginErrorMessageIsDisplayed());
    }

    @Then("User checks that passwordErrorMessage is displayed")
    public void userChecksPasswordErrorMessageIsDisplayed() {
        assertTrue(signUpPage.passwordErrorMessageIsDisplayed());
    }

    @Then("User checks that Please select size message is displayed")
    public void userChecksThatPleaseSelectSizeMessageIsDisplayed() {
        assertTrue(homePage.pleaseSelectSizeMessageIsDisplayed());
    }

    @And("User clicks my bag button")
    public void userClickMyBagButton() {
        homePage.clickMyBagButton();
    }

    @And("User clicks view bag button")
    public void userClickViewBagButton() {
        homePage.clickViewBagButton();
    }

    @And("User changes the quantity of shirts to 6")
    public void userChangesQuantityOfShirtsToSix() {
        myBagPage = pageFactoryManager.getMyBagPage();
        myBagPage.selectQuantityOfShirtsIsSix();
    }

    @Then("User checks that new quantity is displayed")
    public void userChecksThatNewQuantityIsDisplayed() {
        assertTrue(myBagPage.checkQuantityIsSixIsDisplayed());
    }

    @Given("User clicks country button")
    public void userClicksCountryButton() {
        homePage.clickCountryButton();
    }

    @When("User selects new country")
    public void userSelectsNewCountry() {
        homePage.selectNewCountry();
    }

    @And("User clicks update button")
    public void userClicksUpdatePreferencesButton() {
        homePage.clickUpdatePreferencesButton();
    }

    @Then("User checks that new country was selected")
    public void userChecksThatNewCountryWasSelected() {
        assertTrue(homePage.newCountryIsDisplayed());
    }

    @And("User clicks shirts category")
    public void userClicksShirtsCategory() {
        menPage.clickShirtsCategory();
    }

    @And("User clicks first item")
    public void userClicksFirstItem() {
        shirtsPage = pageFactoryManager.getShirtsPage();
        shirtsPage.clickFirstItem();
    }

    @When("User deletes item from bag")
    public void userDeletesItemFromBag() {
        myBagPage = pageFactoryManager.getMyBagPage();
        myBagPage.clickDeleteButton();
    }

    @Then("User checks that bag is empty message is displayed")
    public void userCheckThatBagIsEmptyMessageIsDisplayed() {
        assertTrue(myBagPage.checkBagIsEmptyMessageIsDisplayed());
    }

    @After
    public void tearDown() {
        driver.close();
    }


    @And("User clicks first price item")
    public void userClicksFirsItem() {
        shirtsPage = pageFactoryManager.getShirtsPage();
        shirtsPage.clickFirstItem();
    }
}
