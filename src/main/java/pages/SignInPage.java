package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage{

    @FindBy(xpath = "//input[@data-st-field='id-signIn-emailAddress']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@data-st-field='id-signIn-password']")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submitButton;

    @FindBy(xpath = "//li[@id='loginErrorMessage']")
    private WebElement loginErrorMessage;

    public SignInPage(WebDriver driver){
        super(driver);
    }

    public void inputEmailAddress(String email) {
        inputTextToField(emailField, email);
    }

    public void inputPassword(String password) {
        inputTextToField(passwordField, password);
    }

    public void clickSignInButton() {
        waitToBeClickableOfElement(submitButton);
        clickElement(submitButton);
    }

    public boolean loginErrorMessageIsDisplayed() {
        return loginErrorMessage.isDisplayed();
    }
}
