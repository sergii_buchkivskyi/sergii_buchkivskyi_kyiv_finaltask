Feature: product testing

  Scenario Outline: Check adding to saved items
    Given User opens '<homepage>' page
    And User clicks men category
    And User clicks clothing category
    And User clicks shirts category
    And User clicks first item
    When User clicks add to saved items button
    And User clicks saved items button
    Then User checks that item is added to saved items

  Examples:
    | homepage         |
    | https://asos.com |

  Scenario Outline: Check 'Please select size' message is displayed after try adding to bag without select size
    Given User opens '<homepage>' page
    And User clicks men category
    And User clicks clothing category
    And User clicks shirts category
    And User clicks first item
    When User clicks add to bag button
    Then User checks that Please select size message is displayed

  Examples:
    | homepage         |
    | https://asos.com |