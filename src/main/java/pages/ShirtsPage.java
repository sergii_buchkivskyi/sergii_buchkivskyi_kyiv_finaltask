package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ShirtsPage extends BasePage{
    @FindBy(xpath = "//article[@data-auto-id='productTile'][1]")
    private WebElement firstItem;

    @FindBy(xpath = "//*[@data-id='sizeSelect']")
    private WebElement sizeSelect;

    @FindBy(xpath = "//button[@aria-label='Add to bag']")
    private WebElement addToBagButton;


    @FindBy(xpath = "//span[@data-auto-id='productTileSaleAmount']")
    private WebElement priceOfItem;

    public ShirtsPage(WebDriver driver){
        super(driver);
    }

    public void clickFirstItem() {
        clickElement(firstItem);
    }

    public void chooseSizeOfShirt(String size) {
        Select select = new Select (sizeSelect);
        select.selectByVisibleText(size);
    }

    public void clickAddToBagButton() {
        clickElement(addToBagButton);
    }

}
