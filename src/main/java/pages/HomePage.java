package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import java.util.List;

import static org.junit.Assert.*;

public class HomePage extends BasePage{

    @FindBy(xpath = "//input[@type='search']")
    private WebElement searchField;

    @FindBy(xpath = "//h2[contains(text(),'NOTHING MATCHES')]")
    private WebElement nothingMatchesYourSearchMessage;

    @FindBy(xpath = "//a[@data-testid='men-floor']")
    private WebElement menCategory;


    @FindBy(xpath = "//a[@aria-label='Saved Items']")
    private WebElement savedItemsButton;

    @FindBy(xpath = "//span[@class='heartPrimary']")
    private WebElement addToSavedItemsButton;

    @FindBy(xpath = "//a[@aria-label='Bag 1 item']")
    private WebElement itemIsAddedToBagIcon;

    @FindBy(xpath = "//div[@id='myAccountDropdown']")
    private WebElement myAccountButton;

    @FindBy(xpath = "//a[@data-testid='signup-link']")
    private WebElement joinLink;

    @FindBy(xpath = "//a[@data-testid='signin-link']")
    private WebElement signInLink;

    @FindBy(xpath = "//span[@id='selectSizeError']")
    private WebElement selectSizeErrorMessage;

    @FindBy(xpath = "//span[@type='bagFilled']")
    private WebElement myBagButton;

    @FindBy(xpath = "//a[@data-test-id='bag-link']")
    private WebElement viewBagButton;

    @FindBy(xpath = "//header//div[@data-testid='country-selector']")
    private WebElement countryButton;

    @FindBy(xpath = "//select[@id='country']")
    private WebElement selectCountry;

    @FindBy(xpath = "//button[@data-testid='save-country-button']")
    private WebElement updatePreferencesButton;

    @FindBy(xpath = "//header//img[@alt='United Kingdom']")
    private WebElement unitedKingdomIcon;

    @FindBy(xpath = "//option[@value='GB']")
    private WebElement unitedKingdom;


    private String listOfSearchedElements ="//article[@id]";


    public HomePage(WebDriver driver) {
        super(driver);
    }


    public void openHomePage(String url) {
        driver.get(url);
    }

    public boolean searchFieldIsVisibility(){
        return searchField.isDisplayed();
    }

    public void inputWordToSearchField(final String searchWord){
        inputTextToField(searchField, searchWord);
    }

    public boolean checkThatUrlContainsSearchWord(String searchQuery){
        return driver.getCurrentUrl().contains(searchQuery);
    }

    public boolean checkNothingMatchesYourSearchMessageIsDisplay(){
        return nothingMatchesYourSearchMessage.isDisplayed();
    }

    public List<WebElement> getSearchedElements(){
        return getListElements(listOfSearchedElements);
    }

    public boolean checkThatAmountOfProductsEqualsAmountFromSearchResultPage(int amount){
        return amount==(getSearchedElements().size());
    }

    public void clickMenCategory(){
        clickElement(menCategory);
    }



    public void clickSavedItemsButton() {
        clickElement(savedItemsButton);
    }


    public boolean itemIsAddedToBag() {
        return itemIsAddedToBagIcon.isDisplayed();
    }

    public void clickAddToSavedItemsButton() {
        clickElement(addToSavedItemsButton);
    }

    public void clickMyAccountButton() {
        waitVisibilityOfElement(myAccountButton);
        clickElement(myAccountButton);
    }

    public void clickJoinLink() {
        waitVisibilityOfElement(joinLink);
        clickElement(joinLink);
    }

    public void clickSignInLink() {
        waitVisibilityOfElement(signInLink);
        clickElement(signInLink);
    }

    public boolean pleaseSelectSizeMessageIsDisplayed() {
        return selectSizeErrorMessage.isDisplayed();
    }

    public void clickMyBagButton() {
        waitToBeClickableOfElement(myBagButton);
        clickElement(myBagButton);
    }

    public void clickViewBagButton() {
        waitToBeClickableOfElement(viewBagButton);
        clickElement(viewBagButton);
    }

    public void clickCountryButton() {
        clickElement(countryButton);
    }

    public void selectNewCountry() {
        waitVisibilityOfElement(selectCountry);
        clickElement(selectCountry);
        waitVisibilityOfElement(unitedKingdom);
        clickElement(unitedKingdom);
    }

    public void clickUpdatePreferencesButton() {
        waitVisibilityOfElement(updatePreferencesButton);
        clickElement(updatePreferencesButton);
    }

    public boolean newCountryIsDisplayed() {
        return unitedKingdomIcon.isDisplayed();
    }
}
