Feature: bag testing

  Scenario Outline: Check adding to bag
    Given User opens '<homepage>' page
    And User clicks men category
    And User clicks clothing category
    And User clicks shirts category
    And User clicks first item
    And User selects '<sizeShirt>' of shirt
    When User clicks add to bag button
    Then User checks that number 1 is appeared on the bag icon

    Examples:
      | homepage         | sizeShirt |
      | https://asos.com | M         |

  Scenario Outline: Check deleting item from bag
    Given User opens '<homepage>' page
    And User clicks men category
    And User clicks clothing category
    And User clicks shirts category
    And User clicks first item
    And User selects '<sizeShirt>' of shirt
    And User clicks add to bag button
    And User clicks view bag button
    When User deletes item from bag
    Then User checks that bag is empty message is displayed

    Examples:
      | homepage         | sizeShirt |
      | https://asos.com | M         |

  Scenario Outline: Check the change in the quantity of goods in the bag
    Given User opens '<homepage>' page
    And User clicks men category
    And User clicks clothing category
    And User clicks shirts category
    And User clicks first item
    And User selects '<sizeShirt>' of shirt
    When User clicks add to bag button
    And User clicks view bag button
    And User changes the quantity of shirts to 6
    Then User checks that new quantity is displayed

    Examples:
      | homepage         |  sizeShirt |
      | https://asos.com |  M         |