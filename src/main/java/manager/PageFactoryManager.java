package manager;
import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {
    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public ShirtsPage getShirtsPage(){
        return new ShirtsPage(driver);
    }

    public MenPage getMenPage() {
        return new MenPage(driver);
    }

    public SavedItemsPage getSavedItemsPage(){
        return new SavedItemsPage(driver);
    }

    public SignUpPage getSignUpPage(){
        return new SignUpPage(driver);
    }

    public SignInPage getSignInPage(){
        return new SignInPage(driver);
    }

    public MyBagPage getMyBagPage(){
        return new MyBagPage(driver);
    }

}
