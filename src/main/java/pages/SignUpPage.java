package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class SignUpPage extends BasePage{

    @FindBy(xpath = "//input[@alt='Email']")
    private WebElement emailAddressField;

    @FindBy(xpath = "//input[@data-st-field='id-register-firstName']")
    private WebElement firstNameField;

    @FindBy(xpath = "//input[@data-st-field='id-register-lastName']")
    private WebElement lastNameField;

    @FindBy(xpath = "//input[@data-st-field='id-register-password']")
    private WebElement passwordField;

    @FindBy(xpath = "//select[@aria-label='Date of birth Day']")
    private WebElement dateOfBirth;

    @FindBy(xpath = "//select[@aria-label='Date of birth Month']")
    private WebElement monthOfBirth;

    @FindBy(xpath = "//select[@aria-label='Date of birth Year']")
    private WebElement yearOfBirth;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement joinAsosButton;

    @FindBy(xpath = "//span[@id='Password-error']" )
    private WebElement passwordErrorMessage;

    public SignUpPage(WebDriver driver){
        super(driver);
    }

    public void inputEmailAddress(String email) {
        inputTextToField(emailAddressField, email);
    }

    public void inputFirstName(String firstName) {
        inputTextToField(firstNameField, firstName);
    }

    public void inputLastName(String lastName) {
        inputTextToField(lastNameField,lastName);
    }

    public void inputPassword(String password){
        inputTextToField(passwordField, password);
    }

    public void selectDataOfBirth(){
        Select selectDate = new Select (dateOfBirth);
        selectDate.selectByVisibleText("1");
        Select selectMonth = new Select (monthOfBirth);
        selectMonth.selectByVisibleText("January");
        Select selectYear = new Select (yearOfBirth);
        selectYear.selectByVisibleText("2000");
    }

    public void clickJoinAsosButton(){
        clickElement(joinAsosButton);
    }

    public boolean passwordErrorMessageIsDisplayed() {
        return passwordErrorMessage.isDisplayed();
    }
}
