package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SavedItemsPage extends BasePage {
    @FindBy (xpath = "//img[contains(@class,'productImage')]")
    private WebElement itemInSavedItems;

    public SavedItemsPage (WebDriver driver){
        super(driver);
    }

    public boolean checkAddingToSavedItems() {
        waitVisibilityOfElement(itemInSavedItems);
        return itemInSavedItems.isDisplayed();
    }

}
