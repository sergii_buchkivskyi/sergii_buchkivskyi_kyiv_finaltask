Feature: account testing

  Scenario Outline: Check sign up with invalid password
    Given User opens '<homepage>' page
    And User clicks myAccount button
    And User clicks join link
    And User inputs email '<address>'
    And User inputs first '<fName>'
    And User inputs last '<lName>'
    And User inputs '<password>'
    And User selects data of birth
    When User clicks join asos button
    Then User checks that passwordErrorMessage is displayed

    Examples:
      | homepage         | address           | fName | lName | password |
      | https://asos.com | fcmalin@gmail.com | epam  | epam  | -        |

  Scenario Outline: Check sign in with invalid password
    Given User opens '<homepage>' page
    And User clicks myAccount button
    And User clicks sign in link
    And User inputs signInEmail '<address>'
    And User inputs signInPassword '<password>'
    When User clicks sign in button
    Then User checks that passwordSignInErrorMessage is displayed

    Examples:
      | homepage         | address           | password |
      | https://asos.com | fcmalin@gmail.com | -        |

  Scenario Outline: Check changing country for shopping
    Given User opens '<homepage>' page
    And User clicks country button
    When User selects new country
    And User clicks update button
    Then User checks that new country was selected

    Examples:
      | homepage         |
      | https://asos.com |