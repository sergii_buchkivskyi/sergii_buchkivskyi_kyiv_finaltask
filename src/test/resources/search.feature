Feature: search field testing

  Scenario Outline: Check that url contains searchQuery
    Given User opens '<homepage>' page
    And User checks search field visibility
    When User makes search by keyword '<searchWord>'
    Then User checks that url contains '<searchQuery>'
    Examples:
      | homepage         | searchWord | searchQuery |
      | https://asos.com | shirts     | q=shirts    |

  Scenario Outline: Check that nothing matches your search message is displayed after input invalid data
    Given User opens '<homepage>' page
    And User checks search field visibility
    When User makes search by keyword '<searchWord>'
    Then User checks that nothing matches your search message is displayed
    Examples:
      | homepage         | searchWord |
      | https://asos.com | -          |

  Scenario Outline: Check that amount of products on search result is correct
    Given User opens '<homepage>' page
    And User checks search field visibility
    When User makes search by keyword '<searchWord>'
    Then User checks that amount of products on search result page are <amountOfSearchResult>
    Examples:
      | homepage         | searchWord | amountOfSearchResult |
      | https://asos.com | Ferrari    | 3                    |